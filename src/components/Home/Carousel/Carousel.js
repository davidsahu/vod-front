import React, { Component, Fragment } from "react";

import Axios from "axios";
import Cards from "../Cards/Cards";
import "./Carousel.scss";
import "../../Loader/Loader.scss";
import { Link, Redirect } from "react-router-dom";

export default class Carousel extends Component {
  state = {
    data: null,
    card: "",
    loading: true,
    redirect: false,
    idCard: "",
    
  };

  async componentDidMount() {
    const data = await Axios.get(`https://vod-accedo-back.herokuapp.com/api/movies`);

    // data.data.entries.map(ele => {
    //   console.log(ele.title);
    // });
    this.setState({
      data: data.data.data,
      card: data.data.data[0],
      loading: false
    });
    console.log("DATA", this.state.data);
  }

  btnNext = () => {
    if (this.state.card.index !== this.state.data.length - 1) {
  
     
        console.log("im in next");
        const newIndex = this.state.card.index + 1;
        if (this.state.data) {
          this.setState({
            card: this.state.data[newIndex]
          });
        }
      
    }
  };

  btnBack = () => {
    if (this.state.card.index !== 0) {
      const newIndex = this.state.card.index - 1;
      if (this.state.data) {
        this.setState({
          card: this.state.data[newIndex]
        });
      }
    }
  };

  nextButton = e => {
    if (this.state.card.index !== this.state.data.length - 1) {
      if (e.keyCode === 39) {
        console.log("im in");
        const newIndex = this.state.card.index + 1;
        if (this.state.data) {
          this.setState({
            card: this.state.data[newIndex]
          });
        }
      }
    }
    if (e.keyCode === 13) {
      console.log("press enter 🕵️‍♂️");
      console.log(this.state.card._id);
      this.setState({ redirect: true, idCard: this.state.card._id });
    }
    if (this.state.card.index !== 0) {
      if (e.keyCode === 37) {
        const newIndex = this.state.card.index - 1;
        if (this.state.data) {
          this.setState({
            card: this.state.data[newIndex]
          });
        }
      }
    }
  };

  backButton = e => {
    if (this.state.card.index !== 0) {
      if (e.keyCode === 37) {
        const newIndex = this.state.card.index - 1;
        if (this.state.data) {
          this.setState({
            card: this.state.data[newIndex]
          });
        }
      }
      if (e.keyCode === 13) {
        console.log("press enter 🕵️‍♂️");
        console.log(this.state.card._id);
        this.setState({ redirect: true, idCard: this.state.card._id });
      }
    }
    if (this.state.card.index !== this.state.data.length - 2) {
      if (e.keyCode === 39) {
        console.log("thunder");
        const newIndex = this.state.card.index + 1;
        if (this.state.data) {
          this.setState({
            card: this.state.data[newIndex]
          });
        }
      }
    }
  };



  render() {
    // console.log('user Id',this.props.user)

    const { data, card, redirect, idCard } = this.state;
    if (this.state.loading === true && !this.data) {
      return <div id="loader"></div>;
    }
    if (redirect) {
      return <Redirect push to={`/movie/${idCard}`} />;
    }

    return (
      <Fragment>
        <h1 className="color-text">Movies</h1>
        {data && card && this.props.user ? (
          <div>
            <div className="btn-next-prev">
              <button
                className="button button-style"
                onKeyUp={this.nextButton}
                onClick={() => this.btnNext()}
                // disabled={card.index === data.length - 1}
              >
                Next
              </button>
              <button
                className="button button-style"
                onKeyUp={this.backButton}
                onClick={this.btnBack}
                // disabled={card.index === 0}
              >
                Prev
              </button>
            </div>
            <Link className="cards-align" to={`/movie/${card._id}`}>
              <div className={`cards-slider active-slide-${card.index}`}>
                <div
                  className="cards-slider-wrapper "
                  style={{
                    transform: `translateX(-${card.index *
                      (100 / data.length)}%)`
                  }}
                >
                  <Cards data={data} user={this.props.user} />
                </div>
              </div>
            </Link>
          </div>
        ) : (
          <div></div>
        )}
      </Fragment>
    );
  }
}
