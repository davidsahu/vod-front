import React, { Component, Fragment } from "react";
import Axios from "axios";
import "./Description.scss";
import "../../components/Loader/Loader.scss"
import { Route, BrowserRouter, Switch } from "react-router-dom";
import Home from "../Home/Home";
import Video from "../Video/Video";

export default class Description extends Component {
  state = {
    data: null,
    isWatching: false,
    history: "",
    user: undefined,
    loading: true
  };

  async componentDidMount() {
    const data = await Axios.get(
      `https://vod-accedo-back.herokuapp.com/api/movies/${this.props.match.params.movieId}`
    );

    const user = await Axios.get(`https://vod-accedo-back.herokuapp.com/api/users`);

    this.setState({ data: data.data.data, user: user.data.data, loading: false });
    console.log(this.state.data);
  }

  async playVideo() {
    const { user } = this.state;
    //this.props.history.push('/video')
    this.setState({ isWatching: true });
    const userId = user[0]._id;
    const history = this.state.data;

    console.log('id',userId);
    // user.map(({history}) => {

    //   if(history.length > 0) {
  
    //   }
    // }
    // )
    // console.log("👼", history);
    Axios.put(`https://vod-accedo-back.herokuapp.com/api/users/${userId}/append/history`, {
      history
    });
  }

  render() {
    const { data, isWatching, loading } = this.state;
    if(loading === true && !data) {
      return <div id="loader"> </div>
    }  
    return (
      <Fragment>
        {data ? (
          <div>
            <div className="container">
              <div className="card">
                <img className="image" src={data.images[0].url} />
                <div className="container"></div>
                <h4>
                  <p>{data.title}</p>
                </h4>
              </div>
              <div className="color-text">
                <h2>{data.title}</h2>
                <p>
                  {`Parental Rating: ${data.parentalRatings[0].rating}`} |{" "}
                  {"Language: " + data.audios.map(lan => `${lan}`)}
                </p>
                <p>{data.description}</p>
                <p>
                  Directores:{" "}
                  {data.credits.map(director => `${director.name}, `)}
                </p>
                <div>
                  <button
                    className="button button-style"
                    onClick={() => this.playVideo()}
                  >
                    Watch video
                  </button>
                </div>
              </div>
            </div>
            <div>
              {isWatching ? (
                 <video
                 autoPlay
                 id="video"
                 controls
                 src={data.contents[0].url}
                 type="video/mp4"
                 onEnded={() => this.props.history.push(`/`)}
               ></video>
              ) : (
                // <Video data={data.contents[0].url} id={data._id} />

                <h2>Info</h2>
              )}
            </div>
          </div>
        ) : (
          <h1>No data</h1>
        )}
      </Fragment>
    );
  }
}
