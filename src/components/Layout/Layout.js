import React, { Fragment } from "react";
import NavBar from "../Home/NavBar/NavBar";

export default function Layout(props) {
  return (
    <Fragment>
      <NavBar />
      {props.children}
    </Fragment>
  );
}
