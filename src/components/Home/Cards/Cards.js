import React, { Component, Fragment } from "react";
import "./Cards.scss";
import { Link } from "react-router-dom";

const Cards = ({ data, user }) => {
  //const { data, card } = data;
  //console.log("user Card", user);
  console.log(data);
  return (
    <Fragment>
      {data ? (
        data.map(movie => (
          <Link   key={movie._id}  to={`/movie/${movie._id}`}>
            <div id={`card-${movie.index}`} className={`card`}>
              <img className="image" src={movie.images[0].url} />
              <h4 className="main-title">{movie.title}</h4>
            </div>
          </Link>
        ))
      ) : (
        <h1>No data</h1>
      )}
    </Fragment>
  );
};

export default Cards;
