import React, { Component, Fragment } from "react";
import "./History.scss";
import Axios from "axios";
import Cards from "../../components/Home/Cards/Cards";
import "../../components/Loader/Loader.scss";
import { Link } from "react-router-dom";

export default class History extends Component {
  state = {
    user: undefined,
    history: undefined,
    card: undefined,
    loading: true
  };
  async componentDidMount() {
    const user = await Axios.get(`https://vod-accedo-back.herokuapp.com/api/users`);
    this.setState({ user: user.data.data });
    console.log("usuario history", this.state.user);
    this.state.user.map(({ history }) => {
      //console.log(history)
      this.setState({ history: history, card: history[0], loading: false });
    });
  }


  render() {
    const { history, card, loading } = this.state;
    if (loading === true && !history) {
      return <div id="loader"></div>;
    }
    return (
      <Fragment>
        <h1 className="color-text">Watched Videos</h1>
        {history && card ? (
          <Link className="cards-align" to={`/movie/${card._id}`}>
            <div className={` active-slide-${card.index} card-space`}>
              {/* <div className="space-cards"> */}
              <Cards data={history} />
              {/* </div> */}
            </div>
          </Link>
        ) : (
          <div className="color-text">
            <h3>You have not watched any videos yet.</h3>
          </div>
        )}
      </Fragment>
    );
  }
}
