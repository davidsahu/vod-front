import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./NavBar.scss";
import Axios from "axios";

export default class NavBar extends Component {
  state = {
    users: undefined
  };

  async componentDidMount() {
    const user = await Axios.get(
      `https://vod-accedo-back.herokuapp.com/api/users`
    );
    this.setState({ user: user.data.data });
  }

  render() {
    const { user } = this.state;
    return (
      <div className="NavBar">
        <div className="container-fluid">
        {user ? (
              <div style={{display:'flex'}}>
                <div style={{paddingTop: '12%', paddingLeft:'10px'}}>
                <img
                  className="profile-image"
                  src={user.map(({ profilePicture }) => profilePicture)}
                />
                </div>
                <div style={{paddingLeft:'5px', paddingTop:'5px'}}>
                <p className="txt-profile">{user.map(({userName})=>userName)}</p>
                
                </div>
              </div>
            ) : (
              <p></p>
            )}
          {/* <img className="Navbar__brand-logo" src={logo} alt="Logo" /> */}
          <Link
            className="Navbar-brand"
            to="/"
            style={{ textDecoration: "none" }}
          >
            <span className="font-weight-light">Video on Demand</span>
            <span className="font-weight-bold">Online</span>
          </Link>
          <div className="btn-direction">
            <Link
              className="Navbar-brand"
              to="/"
              style={{ textDecoration: "none" }}
            >
              <button className="button button-style">Home</button>
            </Link>
            <Link
              className="Navbar-brand"
              to={"history"}
              style={{ textDecoration: "none" }}
            >
              <button className="button button-style ">History</button>
            </Link>
           
          </div>
        </div>
      </div>
    );
  }
}
