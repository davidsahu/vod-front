import React, { Component, Fragment } from "react";
import Carousel from "../../components/Home/Carousel/Carousel";
import Axios from "axios";

class Home extends React.Component {
  state = {
    data: undefined
  };

  async componentDidMount() {
    const data = await Axios.get(`https://vod-accedo-back.herokuapp.com/api/users`);
    this.setState({ data: data.data.data });
    // console.log('users',this.state.data)

  }

  render() {
      const {data} = this.state
    return (
      <Fragment>
        <Carousel user={data}/>
      </Fragment>
    );
  }
}

export default Home;
