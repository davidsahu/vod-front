import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Home from "./views/Home/Home";
import Description from "./views/Description/Description";
import History from "./views/History/History";
import Video from "./views/Video/Video";
import Layout from "./components/Layout/Layout";
import Notfound from "./views/NotFound/Notfound";

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/history" component={History} />
          <Route exact strict path="/movie/History" component={History} />
          <Route exact strict path="/movie/:movieId" component={Description} />
         
          <Route exact path="/video" component={Video} />
          <Route component={Notfound} />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
